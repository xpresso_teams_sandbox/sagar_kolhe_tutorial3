"""
This is the implementation of training for rossman store sales prediction
"""

__author__ = "Naveen Sinha"

import os
import sys

import pandas as pd
import tensorflow as tf
import tensorflow.compat.v1.keras.backend as K
from sklearn.model_selection import train_test_split
from tensorflow.keras.layers import Input, Embedding, Concatenate, Dense, \
    Flatten, Reshape, BatchNormalization, Dropout

from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.INFO)

DEFAULT_BATCH_SIZE = 128
DEFAULT_TRAIN_EPOCHS = 10

logging = XprLogger("rossman_sales_project")


class LossAndErrorPrintingCallback(tf.keras.callbacks.Callback):
    """ This block sets up a hook into the Keras library, which enables
    the status to be reported back to the Controller at the end of each epoch. """

    def __init__(self, dnn_training_object):
        super().__init__()
        self.dnn_obj = dnn_training_object

    def on_epoch_end(self, epoch, logs=None):
        """ This is called at the end of every epoch. In this logs
        we can get different metrics that were recorded.
        We are sending the required petrics to the xpresso controller"""
    
        logging.info(
            'The average loss for epoch {} is {:7.2f}.'.format(epoch,
                                                               logs['loss']))
        logging.info(logs)
        report_status = {
            "status": {
                "status": "Epoch {}".format(epoch)
            },
            "metric": {
                "loss": logs["loss"],
                "epoch": epoch
            }
        }
        logging.info(report_status)
        self.dnn_obj.report_status(status=report_status)


class DnnTrain(AbstractPipelineComponent):
    """ This is the main class which performs the model training
    This class performs following steps:
        1. Build vocabulary
        2. Builds the tensor
        3. Builds the keras dense neural network model
        4. Trains the model
        5. Stores the model locally
        6. Pushes the model to the xpresso versioning system automatically
    """

    def __init__(self, component_name, base_folder="/data/sales_data"):
        """ Initializes difference variables being used in the class"""
    
        super().__init__(name=component_name)
        self.base_folder = base_folder
        self.split_train_path = os.path.join(self.base_folder,
                                             "split_train.csv")
        self.split_valid_path = os.path.join(self.base_folder,
                                             "split_valid.csv")
        self.orig_train = None

        self.train = None
        self.valid = None
        self.feature_names = None
        self.csv_defaults = None

    def load(self):
        """ Load precessed dataset into a member variable"""    
        self.orig_train = pd.read_csv(
            os.path.join(self.base_folder,
                         "dnn_combined_train.csv"))

    def build_vocabulary(self, df: pd.DataFrame, cols):
        """ Iterates through each columns and build the vocabulary
        for encoding
        Returns:
            dict: vocabulary dictionary
        """    
        vocab = {}
        for col in cols:
            values = df[col].unique()
            col_type = type([x for x in values if x is not None][0])
            default_value = col_type()
            vocab[col] = sorted(values, key=lambda x: x or default_value)
        return vocab

    def cast_columns(self, df: pd.DataFrame, cols):
        """ Type cast columns into float

         Returns:
            pandas.Dataframe
        """
        for col in cols:
            df[col] = df[col].astype(float)
        return df

    def lookup_columns(self, df, vocab):
        """ Create vocabulary dictionary reverse lookup for each columns

        Returns:
            pandas.Dataframe
        """
        for col, mapping in vocab.items():
            df[col] = df[col].apply(mapping.index)
            df[col] = df[col].astype(float)
        return df

    def exp_rmspe(self, y_true, y_pred):
        """Competition evaluation metric RMSPE. It expects logarthmic inputs.
        Returns:
            float: Root Mean Square percentage error
        """
        pct = tf.square((tf.exp(y_true) - tf.exp(y_pred)) / tf.exp(y_true))
        # Compute mean excluding stores with zero denominator.
        x = tf.reduce_sum(tf.where(y_true > 0.001, pct, tf.zeros_like(pct)))
        y = tf.reduce_sum(
            tf.where(y_true > 0.001, tf.ones_like(pct), tf.zeros_like(pct)))
        return tf.sqrt(x / y)

    def act_sigmoid_scaled(self, x):
        """Sigmoid scaled to logarithm of maximum sales scaled by 20%."""
        return tf.nn.sigmoid(x) * tf.compat.v1.log(42000.00) * 1.2

    def start(self, run_name, batch_size=DEFAULT_BATCH_SIZE, train_eopchs=DEFAULT_TRAIN_EPOCHS):
        """ Perform data encoding of the dataset and trains the model.
        Once the data has been trained, it persists the model in
        self.OUTPUT_DIR.
        It calls super().completed(push_exp=True) to version the model in
        the xpresso versioning system
        """
        super().start(xpresso_run_name=run_name)
        self.batch_size = batch_size
        self.train_eopchs = train_eopchs
        logging.info("preparing tensor")
        categorical_cols = ['Store', 'DayOfWeek', 'Promo', 'StateHoliday',
                            'SchoolHoliday', 'StoreType', 'Assortment',
                            'Promo2', 'Day', 'Month', 'Year', 'isCompetition']

        numeric_cols = ['CompetitionDistance']

        all_cols = categorical_cols + numeric_cols
        CUSTOM_OBJECTS = {'exp_rmspe': self.exp_rmspe,
                          'act_sigmoid_scaled': self.act_sigmoid_scaled}

        self.max_sales = float(self.orig_train['Sales'].max())
        vocab = self.build_vocabulary(self.orig_train[categorical_cols],
                                      categorical_cols)
        self.orig_train = self.cast_columns(self.orig_train,
                                            numeric_cols + ['Sales'])
        self.orig_train = self.lookup_columns(self.orig_train, vocab)

        train, valid = train_test_split(self.orig_train, test_size=0.05,
                                        random_state=42)
        train.to_csv("/data/sales_data/split_train.csv")
        valid.to_csv("/data/sales_data/split_valid.csv")
        # train = pd.read_csv("/data/sales_data/split_train.csv")
        # valid = pd.read_csv("/data/sales_data/split_valid.csv")
        logging.info(train.head())
        logging.info(valid.head())
        logging.info("Starting session")
        config = tf.compat.v1.ConfigProto(device_count={'GPU': 0})
        K.set_session(tf.compat.v1.Session(config=config))

        # Build the model.
        logging.info("Building model")
        inputs = {col: Input(shape=(1,), name=col) for col in all_cols}
        embeddings = [
            Embedding(len(vocab[col]), 10, input_length=1, name='emb_' + col)(
                inputs[col])
            for col in categorical_cols]
        continuous_bn = Reshape((1, 1), name='reshape_' + numeric_cols[0])(
            inputs[numeric_cols[0]])
        continuous_bn = BatchNormalization()(continuous_bn)
        x = Concatenate()(embeddings + [continuous_bn])
        x = Flatten()(x)
        x = Dense(1000, activation='relu',
                  kernel_regularizer=tf.keras.regularizers.l2(0.00005))(x)
        x = Dense(1000, activation='relu',
                  kernel_regularizer=tf.keras.regularizers.l2(0.00005))(x)
        x = Dense(1000, activation='relu',
                  kernel_regularizer=tf.keras.regularizers.l2(0.00005))(x)
        x = Dense(500, activation='relu',
                  kernel_regularizer=tf.keras.regularizers.l2(0.00005))(x)
        x = Dropout(0.5)(x)
        output = Dense(1, activation=self.act_sigmoid_scaled)(x)
        model = tf.keras.Model([inputs[f] for f in all_cols], output)
        model.summary()
        opt = tf.keras.optimizers.Adam(lr=1e-4, epsilon=1e-3)
        model.compile(opt, 'mae', metrics=[self.exp_rmspe])

        logging.info("Training model")
        history = model.fit(
            x=[tf.convert_to_tensor(train[col].values) for col in all_cols],
            y=[tf.convert_to_tensor(train['Sales'].values)],
            callbacks=[LossAndErrorPrintingCallback(self)],
            verbose=2,
            batch_size=self.batch_size,
            validation_steps=len(valid) / self.batch_size,
            steps_per_epoch=len(train) / self.batch_size,
            epochs=self.train_eopchs,
            validation_data=
            ([tf.convert_to_tensor(valid[col].values) for col in all_cols],
             [tf.convert_to_tensor(valid['Sales'].values)]))
        logging.info(history)
        logging.info("Saving model")
        if not os.path.exists(self.OUTPUT_DIR):
            os.makedirs(self.OUTPUT_DIR)
        model.save(os.path.join(self.OUTPUT_DIR, 'saved_model.h5'))
        logging.info("Saved model... load model")

        super().completed(push_exp=True)


if __name__ == "__main__":   
    """ Main file to start the execution 
        It takes run_id parameters from command line"""
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/app.py

    trainer = DnnTrain(component_name="DNN_Train",
                          base_folder="/data/sales_data/")
    trainer.load()
    trainer.start(run_name=sys.argv[1], batch_size=int(sys.argv[2]), train_eopchs=int(sys.argv[3]))
